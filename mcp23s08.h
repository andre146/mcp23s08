#ifndef MCP23S08_H
#define MCP23S08_H

#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include "megaspi/megaSPI.h"
#include "megaspi/megaSPI.c"

#define EXP_CS_PORT PORTC
#define EXP_CS_PIN PC3

#define EXP_ADDR_IODIR 0x00
#define EXP_ADDR_IPOL 0x01
#define EXP_ADDR_GPINTEN 0x02
#define EXP_ADDR_DEFVAL 0x03
#define EXP_ADDR_INTCON 0x04
#define EXP_ADDR_IOCON 0x05
#define EXP_ADDR_GPPU 0x06
#define EXP_ADDR_INTF 0x07
#define EXP_ADDR_INTCAP 0x08
#define EXP_ADDR_GPIO 0x09
#define EXP_ADDR_OLAT 0x0A

#define MCP_WRITE 0  
#define MCP_READ 1

extern void mcp23_write_reg_once(uint8_t device_addr, uint8_t reg_addr, uint8_t data);

extern uint8_t mcp23_read_reg_once(uint8_t device_addr, uint8_t reg_addr);

/*selects the device and addresses it, but does not deselect it to enable consecutive writes or reads*/
extern void mcp23_start(uint8_t device_addr, uint8_t reg_addr, uint8_t rw);

/*deselcts the device after multiple consecutive read or write operations*/
extern void mcp23_stop();

/*writes to a device after it has been selected by mcp23_start*/
extern void mcp23_write_reg(uint8_t data);

/*reads a device after it has been selected by mcp23_start*/
extern uint8_t mcp23_read_reg();

#endif /* MCP23S08_H */

