#include "mcp23s08.h"

void mcp23_write_reg_once(uint8_t device_addr, uint8_t reg_addr, uint8_t data){
    EXP_CS_PORT &= ~(1<<EXP_CS_PIN); //select device
    spi_transmit_byte(device_addr | MCP_WRITE);  //send device address + W
    spi_transmit_byte(reg_addr); //Select the register
    spi_transmit_byte(data); //write data into the register
    EXP_CS_PORT |= (1<<EXP_CS_PIN); //deselect device
}

uint8_t mcp23_read_reg_once(uint8_t device_addr, uint8_t reg_addr){
    uint8_t response = 0;
    EXP_CS_PORT &= ~(1<<EXP_CS_PIN); //select device
    spi_transmit_byte(device_addr);  //send device address + W
    spi_transmit_byte(reg_addr); //Select the register
    response = spi_transmit_byte(0x00); //write zeros to the device and save the response
    EXP_CS_PORT |= (1<<EXP_CS_PIN); //deselect device
    return(response);
}

/*selects the device and addresses it, but does not deselect it to enable consecutive writes or reads*/
void mcp23_start(uint8_t device_addr, uint8_t reg_addr, uint8_t rw){ 
    EXP_CS_PORT &= ~(1<<EXP_CS_PIN); //select device
    spi_transmit_byte(device_addr | rw);  //send device address + W
    spi_transmit_byte(reg_addr); //Select the register
}

/*deselects the device after multiple consecutive read or write operations*/
void mcp23_stop(){ 
    EXP_CS_PORT |= (1<<EXP_CS_PIN);
}

/*writes to a device after it has been selected by mcp23_start*/
void mcp23_write_reg(uint8_t data){
    spi_transmit_byte(data);
}

/*reads a device after it has been selected by mcp23_start*/
uint8_t mcp23_read_reg(){
    return(spi_transmit_byte(0x00));    
}